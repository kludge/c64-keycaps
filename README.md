![Open source keycaps for your Commodore 64. Ready to 3D print, or remix.](Images/C64_keycaps_flyer_small.png)

# Commodore 64 keycaps

Open source keycap replacements for the Commodore 64. They are pretty close to the originals in shape and size, maybe apart from the spacebar, but I think that works as well. Below is an example of a wide key from row 3 compared to an original one.

![Top side of a printed key beside the F5/F6 key.](Images/row_3_wide_top_side.jpg "Top side") ![Bottom side of a printed key beside the F5/F6 key.](Images/row_3_wide_bottom_side.jpg "Bottom side")

If you just want to start printing some, [download the STL pack](STL/v1.1/C64_keys_STL_v1.1.zip). It includes standard and wide keys for rows 1-4, as well as the return key and spacebar. Be aware that the keys are blank, so no text is printed on them. PRINT WITH SUPPORT UNDER THE KEY, OR YOU WILL HAVE A MESS! :D

If you want to tinker with it, [download the F3D](Commodore_64_replacement_keys_v1.1.f3d) and start playing in Fusion 360. The design is somewhat parametric, but I don't really know what I'm doing, so some things are probably really weird. The slope of the keys are configured through user parameters (accessed via _Modify -> Change Parameters_). The two that controls the slope are `back_height` and `front_height`. Only _standard_ and _wide_ keys are affected by these parameters. To change the slope of the return key and spacebar you'd have to change the values in the _Key profile_ sketch of these components.

The values I use are as follows:

| Key row | back_height | front_height | Comment                          |
| ------- | ----------- | ------------ | -------------------------------- |
| Row 1   | 15.3 mm     | 15.3 mm      | Includes F1/F2                   |
| Row 2   | 12.7 mm     | 13.8 mm      | Includes F3/F4                   | 
| Row 3   | 11.4 mm     | 13.9 mm      | Includes F5/F6 as well as Return |
| Row 4   | 11.4 mm     | 15.1 mm      | Includes F7/F8                   |

I have done test prints of some of them, but I can't really guarantee that they will fit. For one, it depends on the calibration of your (and my) 3D printer.

Enjoy!

![3D render of a complete set of keys](Images/C64_keyboard_render.png "3D render of a complete set of keys")

## Version history
| Version | Date.        | Comment                                     |
| ------- | ------------ | ------------------------------------------- |
| 1.0     | Sep 26, 2020 | Initial release.                            |
| 1.1     | Oct 1, 2020  | Second release. The keys sits 0.5 mm lower. |

The files are (C) 2020 by Kludge and are released under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode). 
